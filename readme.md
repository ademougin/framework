###DEMOUGIN ALBAN

#Sources
[https://bitbucket.org/ademougin/framework]()

#Rendu
[https://webetu.iutnc.univ-lorraine.fr/www/demougin4u/FRAMEWORK/]()

#Fonctionnalités implantées : 
- Responsive en 3 dimensions : mobile, tablette et desktop
- Reset ==(_reset.scss)==
- Fichier de variables par défaut ==(_default.scss)==
- Fichier d'adaptation pour votre site ==(_main.scss)==
- Header ==(_composants.scss)==
- Navbar responsive ==(_composants.scss)==
- Fil d'arianne ==(_composants.scss)==
- Titres et sous titres ==(_typos.scss)==
- Balises de texte inline ==(_typos.scss)==
- Quote & blockquote ==(_typos.scss)==
- Listes ==(_typos.scss)==
- Alignements ==(_typos.scss)==
- Transformations de texte ==(_typos.scss)==
- Tableaux ==(_typos.scss)==
- Boutons ==(_buttons.scss)==
- Alertes ==(_alerts.scss)==
- Jumbotron ==(_composants.scss)==
- Pagination ==(_composants.scss)==
- Footer ==(_composants.scss)==
- Utilisable avec ma grille ==(_gridbuilder.scss)==

#Installation / Usage : 
- Ouvrir le framework sur votre navigateur pour un apercu des rendus.
- A la place du CSS de votre site : copier le CSS ou compiler Style.scss (apres avoir édité le fichier afin de sélectionner quelles fonctions l'on souhaite).
- Afin de garder les balises sémantiques du HTML, dans la classe _main on fait le lien entre les balises HTML et les éléments du CSS.
- Pour utiliser la grille, il faut décommenter la ligne du mixin dans Style.scss et eventuellement modifier les dimensions de celle-ci dans _default.scss